A React.js boilerplate template to jumpstart production-ready applications with an emphasis on performance and best practices.
Forked and modified from https://github.com/react-boilerplate/react-boilerplate.

# Quick start

1. Fork this repo by clicking the '+' icon on the left and clicking "Fork this repository"    

2. Clone this repo (name might have changed in fork process)  
  
    git clone https://bitbucket.org/dynamotechnologies/dynamo-react-boilerplate.git 
  
3. Move to the appropriate directory  
  
    cd dynamo-react-boilerplate  
  
4. Install dependencies and clean the git repo  
    
    npm run setup  
  
5. Start dev server to see the example app at `http://localhost:3000`  
    
    npm start  
  
6. Delete the example app  
    
    npm run clean  

# Features

## Core
- React
- React Router
- Redux
- Redux Saga
- Reselect
- ImmutableJS
- Styled Components

## Unit Testing
- Jest
- Enzyme

## Linting
- ESLint

## Debugging
- Redux Logger
- Why Did You Update?

# Commands

## Development Server
Start development server and make your app accessible at localhost:3000 (by default), hot-reloading any changes to the code  
  
`npm start`

## Production Server
Runs tests, builds the app, and starts the production server  
  
`npm run start:production`

## Generators
Use this commnand to auto-generate boilerplate code for common parts of your application. The command will walk you through which files you would like to add  
  
`npm run generate`

## Building
Optimizes and minimizes all files, piping them to the build folder  
  
`npm run build`

## Testing
Run all unit tests specified in the **/tests/*.js files throughout the applications  
  
`npm test`

Run tests specifically for a certain applications (e.g. Button)  
  
`npm test -- Button`

Watch changes to application and re-run tests  
  
`npm run test:watch`


## Linting
Lint your JavaScript code  
  
`npm run lint`


## Documentation
- [**The Hitchhikers Guide to `react-boilerplate`**](docs/general/introduction.md): An introduction for newcomers to this boilerplate.
- [Overview](docs/general): A short overview of the included tools
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.
- [**Troubleshooting**](docs/general/gotchas.md): Solutions to common problems faced by developers.


